// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "UnrealSmoothing.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealSmoothing, "UnrealSmoothing" );

DEFINE_LOG_CATEGORY(LogUnrealSmoothing)
 
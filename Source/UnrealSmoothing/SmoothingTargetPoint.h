// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "SmoothingTargetPoint.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSMOOTHING_API ASmoothingTargetPoint : public ATargetPoint
{
	GENERATED_BODY()
	
		//Real speed in this position from the telemetry file
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Debug, meta = (AllowPrivateAccess = "true"))
		float ExpectedSpeed;
	
	
};

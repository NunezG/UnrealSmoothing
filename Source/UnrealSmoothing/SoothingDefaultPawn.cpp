// Fill out your copyright notice in the Description page of Project Settings.

#include "SoothingDefaultPawn.h"
#include "GameFramework/SpringArmComponent.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "GameFramework/PawnMovementComponent.h"
#include "KismetDebugUtilities.h"
#include "GameFramework/GameMode.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"

#include "Camera/CameraComponent.h"


void ASoothingDefaultPawn::BeginPlay()
{
	Super::BeginPlay();

	UnchangedPoint = GetActorLocation();
	NextPoint = GetActorLocation();

		(GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("BEGIPLAYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY")));


}




void ASoothingDefaultPawn::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);
	//const FVector OldLocation = GetActorLocation();

	//DrawDebugComponents();

	if (Role == ROLE_SimulatedProxy)
	{
		LastServerTimeSeconds = ServerTimeSeconds;

		ServerTimeSeconds = GetWorld()->TimeSeconds;

		UKismetSystemLibrary::DrawDebugSphere(
			GetWorld(),
			NextPoint,
			80,
			12,
			FColor(255, 0, 0)
		);



		//DrawDebugComponents();

		UKismetSystemLibrary::DrawDebugSphere(
			GetWorld(),
			UnchangedPoint,
			60,
			12,
			FColor(0, 0, 255)
		);







		//if (oldReplicates.Num() > 0 )
		//	{
		FVector previousLocation = UnchangedPoint;// oldReplicates[0].Location;
		FVector nextLoccation = NextPoint;


		//}



		if (GetActorLocation() == nextLoccation)
		{
			UE_LOG(LogTemp, Warning, TEXT("RESET DELTA %f"), deltaLerp);

			startMoving = true;
			deltaLerp = 0;
		}


		if (startMoving)
		{

			totalTime = totalTime + DeltaSeconds;
			//Updates current time
			//	time = time + FTimespan::FromSeconds(DeltaSeconds);
			//	FVector nextLocation;

			//Time to next point
			//FTimespan mydeltatime = nextTarget->timeFromGPS - time;


			//NetUpdateFrequency

			//Time between previous and next telemetry targets
			//FTimespan deltaTargets = nextTarget->timeFromGPS - previousTarget->timeFromGPS;

			//Proportion of actual time between previous and next telemetry targets
			//float realDeltaTime = myTimeSeconds - LastTimeSeconds;
			LastTimeSeconds = myTimeSeconds;
			//	myTimeSeconds = GetWorld()->TimeSeconds;

				//remaining Time to next server point
			float  timeRemaining = ServerTimeSeconds - totalTime;

			//	if (timeRemaining < 0)
				//	timeRemaining = -timeRemaining;

				//deltaLerp = deltaLerp + (DeltaSeconds  * NetUpdateFrequency);
				//deltaLerp = deltaLerp + DeltaSeconds;	 

				//Time between previous and next server targets
			float deltaServer = ServerTimeSeconds - LastServerTimeSeconds;

			//	if (deltaServer < 0)
				//	deltaServer = -deltaServer;

			UE_LOG(LogTemp, Warning, TEXT("myTimeSeconds %f"), GetWorld()->TimeSeconds);

			UE_LOG(LogTemp, Warning, TEXT("totaltime %f"), totalTime);

			UE_LOG(LogTemp, Warning, TEXT("Server %f"), ServerTimeSeconds);
			UE_LOG(LogTemp, Warning, TEXT("%f %f"), timeRemaining, deltaServer);


			//Proportion between remaining time and total time between targets
			float proportion = ((timeRemaining) / (deltaServer));

			UE_LOG(LogTemp, Warning, TEXT("proportion %f"), proportion);

			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, deltaLerp);
			//UnchangedPoint
			//Current location between previous and next telemetry targets
			FVector location = FMath::Lerp(previousLocation, nextLoccation, 1- proportion);
			//		speed = FMath::Lerp(previousTarget->ExpectedSpeed, nextTarget->ExpectedSpeed, 1 - DeltaSeconds);
			UE_LOG(LogTemp, Warning, TEXT("location %s"), *ReplicatedMovement.Location.ToString());

			SetActorLocation(location);
			ReplicatedMovement.Location = location;

			
			
			/*
			UKismetSystemLibrary::DrawDebugSphere(
			GetWorld(),
			location,
			120,
			12,
			FColor(255, 255, 0)
			);
			*/
		}

	} 

}


ASoothingDefaultPawn::ASoothingDefaultPawn()
{

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;


	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


															 // Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}


//////////////////////////////////////////////////////////////////////////
// Input

void ASoothingDefaultPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	//check(PlayerInputComponent);

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind movement events
	//PlayerInputComponent->BindAxis("MoveForward", this, &ASoothingDefaultPawn::MoveForward);
//	PlayerInputComponent->BindAxis("MoveRight", this, &ADefaultPawn::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	//PlayerInputComponent->BindAxis("Turn", this, &ADefaultPawn::AddControllerYawInput);
//	PlayerInputComponent->BindAxis("TurnRate", this, &ADefaultPawn::TurnAtRate);
	//PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
//	PlayerInputComponent->BindAxis("LookUpRate", this, &ADefaultPawn::LookUpAtRate);
}

void ASoothingDefaultPawn::MoveForward(float Rate)
{
	//Super::MoveForward(Rate);

		


	if (GetNetMode() != NM_DedicatedServer)
	{

		//GetMovementComponent()->forward
		if (Rate != 0)
		{

		
			if (Role != ROLE_SimulatedProxy)
			{
				Super::MoveForward(Rate);
				

				UKismetSystemLibrary::DrawDebugSphere(
					GetWorld(),
					ReplicatedMovement.Location,
					100,
					12,
					FColor(0, 255, 0)
				);

				/*	if (Controller)
					{
						FRotator const ControlSpaceRot = Controller->GetControlRotation();
						FVector resultVector = FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::X);

						// transform to world space and add it
						AddMovementInput(resultVector, Rate);
					}*/
				
				ServerMoveForward(GetActorLocation());

			}

				SetActorLocation(GetActorLocation());

				//	(GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SERVERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR")));
		
			//else MulticastMoveForward(Rate);
		}
	}
}


void ASoothingDefaultPawn::MoveRight(float Rate)
{
	Super::MoveRight(Rate);

	//	if (Role != ROLE_Authority)
//	ServerMoveForward(GetActorLocation());
	//else MulticastMoveForward(Rate);
}



void ASoothingDefaultPawn::ServerMoveForward_Implementation(FVector Rate)
{
	//Super::MoveForward(Rate);
	//MoveForward(Rate);
	//(GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SERVERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR")));
	
//	MulticastMoveForward(Rate);
	//SetActorLocation(Rate);
	//RootComponent->AddRelativeLocation

	//MulticastMoveForward(Rate);
	FTransform trans;
	//ReplicatedMovement.AngularVelocity
	                               //ReplicatedMovement.Location = Rate;
	//ReplicatedMovement

}

void ASoothingDefaultPawn::MulticastMoveForward_Implementation(float Rate)
{
	//MoveForward(Rate);

//	if(Role == ROLE_SimulatedProxy)
	//	Super::MoveForward(Rate);

	//(GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("MulticastMoveForward_Implementation")));

}




bool ASoothingDefaultPawn::ServerMoveForward_Validate(FVector Rate)
{
	return true;
}


void ASoothingDefaultPawn::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASoothingDefaultPawn::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}



void ASoothingDefaultPawn::PostNetReceiveLocationAndRotation()
{



	//DrawDebugComponents();

	
	UE_LOG(LogTemp, Warning, TEXT("%s PostNetReceiveLocationAndRotationBEFORE %s"), *GetName(), *GetActorLocation().ToString());

	Super::PostNetReceiveLocationAndRotation();

	UE_LOG(LogTemp, Warning, TEXT("%s PostNetReceiveLocationAndRotation AFTER %s"), *GetName(), *GetActorLocation().ToString());


	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PostNetReceiveLocationAndRotation"));



	//OldLocation = GetActorLocation();



}


void ASoothingDefaultPawn::PostNetReceiveVelocity(const FVector& PostNetReceiveVelocity)
{
	
		Super::PostNetReceiveVelocity(PostNetReceiveVelocity); 
		UE_LOG(LogTemp, Warning, TEXT("%s PostNetReceiveVelocity %s"), *GetName(), *GetActorLocation().ToString());
	//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PostNetReceiveVelocity!"));

//	DrawDebugComponents();
}

void ASoothingDefaultPawn::PostNetReceive()
{

	Super::PostNetReceive();
	UE_LOG(LogTemp, Warning, TEXT("%s PostNetReceive %s"), *GetName(), *GetActorLocation().ToString());

//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PostNetReceive!"));

}



void ASoothingDefaultPawn::PreNetReceive()
{

	Super::PreNetReceive();
	UE_LOG(LogTemp, Warning, TEXT("%s PreNetReceive %s"), *GetName(), *GetActorLocation().ToString());

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PreNetReceive!"));


}



void ASoothingDefaultPawn::GatherCurrentMovement()
{

	Super::GatherCurrentMovement();

	///if (Role != ROLE_SimulatedProxy)
	//{

	//	ServerTimeSeconds = GetWorld()->TimeSeconds;

	//	UE_LOG(LogTemp, Warning, TEXT("%f GatherCurrentMovement authority %f"), LastServerTimeSeconds, ServerTimeSeconds = GetWorld()->TimeSeconds);

	//}else

	

	//DrawDebugSphere(UObject* WorldContextObject, const FVector Center, float Radius = 100.f, int32 Segments = 12, FLinearColor LineColor = FLinearColor::White, float Duration = 0.f, float Thickness = 0.f);

}

/** ReplicatedMovement struct replication event */
void ASoothingDefaultPawn::OnRep_ReplicatedMovement()
{

	UE_LOG(LogTemp, Warning, TEXT("%s OnRep_ReplicatedMovement BEFFFF  %s"), *GetName(), *GetActorLocation().ToString());

	UE_LOG(LogTemp, Warning, TEXT("%sReplicatedMovement.Location %s"), *GetName(), *ReplicatedMovement.Location.ToString());


		if (pair % 3 == 0.f)
		{
			UE_LOG(LogTemp, Warning, TEXT("%s REALLLGatherCurrentMovement!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"), *GetName());

			//DrawDebugComponents();



		}
		pair++;

		UKismetSystemLibrary::DrawDebugSphere(
			GetWorld(),
			UnchangedPoint,
			60,
			12,
			FColor(0, 0, 255)
		);


		

		UnchangedPoint = GetActorLocation();


		Super::OnRep_ReplicatedMovement();


		NextPoint = ReplicatedMovement.Location;

		oldReplicates.Add(ReplicatedMovement);

		

		if (oldReplicates.Num() > 5)
			oldReplicates.RemoveAt(0);


		UKismetSystemLibrary::DrawDebugCylinder(
			GetWorld(),
			UnchangedPoint,
			NextPoint,
			10,
			24,
			FLinearColor::Red,
			0.3F
		);


	//	SetActorLocation(UnchangedPoint);

		//DrawDebugComponents();


	//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("OnRep_ReplicatedMovement!"));
		UE_LOG(LogTemp, Warning, TEXT("%s OnRep_ReplicatedMovement  AFFFTT %s"), *GetName(), *GetActorLocation().ToString());


	//	UE_LOG(LogTemp, Warning, TEXT("%s OnRep_ReplicatedMovement AFTER %s"), *GetName(), *GetActorLocation().ToString());

	//	UE_LOG(LogTemp, Warning, TEXT("%sReplicatedMovement.Location AFTER %s"), *GetName(), *ReplicatedMovement.Location.ToString());


		/*/
		if (Role == ROLE_SimulatedProxy)
			//	
			//	else
		{
			(GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("FUCKING PROXYYYYYYYYYYYYYYYYYYY")));

			SetActorLocation(GetActorLocation());
		}
		*/



}

/*
UPawnMovementComponent* ASoothingDefaultPawn::GetMovementComponent() const
{
	return MovementComponent;
}


*/


void ASoothingDefaultPawn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASoothingDefaultPawn, ServerTimeSeconds);

}
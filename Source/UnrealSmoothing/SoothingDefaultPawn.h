// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "SoothingDefaultPawn.generated.h"

UENUM()
enum EInterpolationType
{
	None,
	Linear,
	catmullRom,
	Bezier
};


//USTRUCT(BlueprintType)
struct FSampleMove
{
	//GENERATED_USTRUCT_BODY()

	float DeltaTime;
	float SteeringThrow;
	float Throttle;
	float Time;

};

/**
 * 
 */
UCLASS()
class UNREALSMOOTHING_API ASoothingDefaultPawn : public ADefaultPawn
{
	GENERATED_BODY()

	
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

public:
	ASoothingDefaultPawn();

	//virtual void Tick(float DeltaSeconds) override;
	//virtual UPawnMovementComponent* GetMovementComponent() const override;

private:
	/** DefaultPawn movement component */
//	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
//		UPawnMovementComponent* MovementComponent;

public:
	
	//ONLY SIMULATEDPROXY
	virtual void PostNetReceiveLocationAndRotation() override;
	//ONLYSILULATEDPROXY
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

	// Always called immediately after properties are received from the remote.
	virtual void PostNetReceive() override;
	virtual void PreNetReceive() override;

	virtual void GatherCurrentMovement() override;

	/** ReplicatedMovement struct replication event */
	UFUNCTION()
		virtual void OnRep_ReplicatedMovement() override;

	bool startMoving = false;
protected:

	

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	FVector NextPoint;
	FVector UnchangedPoint;



public:
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	 void MoveForward(float Rate) override;
	 void MoveRight(float Rate) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerMoveForward(FVector Rate);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastMoveForward(float Rate);
	int pair;


	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;


	TArray<FRepMovement> oldReplicates;

	float deltaLerp;

	//UPROPERTY(EditAnywhere)
	EInterpolationType interpolation;

	bool FollowAllPoints;

	bool RabbitMode;

	float myTimeSeconds;

	float LastTimeSeconds;

	UPROPERTY(Replicated)
	float ServerTimeSeconds;

	float LastServerTimeSeconds;


	float totalTime;
};

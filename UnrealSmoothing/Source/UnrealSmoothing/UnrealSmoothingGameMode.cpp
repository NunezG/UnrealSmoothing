// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "UnrealSmoothingGameMode.h"
#include "UnrealSmoothingPawn.h"

AUnrealSmoothingGameMode::AUnrealSmoothingGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = AUnrealSmoothingPawn::StaticClass();
}


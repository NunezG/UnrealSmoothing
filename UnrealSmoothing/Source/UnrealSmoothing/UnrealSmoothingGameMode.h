// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealSmoothingGameMode.generated.h"

UCLASS(MinimalAPI)
class AUnrealSmoothingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnrealSmoothingGameMode();
};




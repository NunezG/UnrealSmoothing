// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALSMOOTHING_UnrealSmoothingGameMode_generated_h
#error "UnrealSmoothingGameMode.generated.h already included, missing '#pragma once' in UnrealSmoothingGameMode.h"
#endif
#define UNREALSMOOTHING_UnrealSmoothingGameMode_generated_h

#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_RPC_WRAPPERS
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingGameMode(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingGameMode(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/UnrealSmoothing"), UNREALSMOOTHING_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingGameMode(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingGameMode(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/UnrealSmoothing"), UNREALSMOOTHING_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UNREALSMOOTHING_API AUnrealSmoothingGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealSmoothingGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREALSMOOTHING_API, AUnrealSmoothingGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREALSMOOTHING_API AUnrealSmoothingGameMode(AUnrealSmoothingGameMode&&); \
	UNREALSMOOTHING_API AUnrealSmoothingGameMode(const AUnrealSmoothingGameMode&); \
public:


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREALSMOOTHING_API AUnrealSmoothingGameMode(AUnrealSmoothingGameMode&&); \
	UNREALSMOOTHING_API AUnrealSmoothingGameMode(const AUnrealSmoothingGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREALSMOOTHING_API, AUnrealSmoothingGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnrealSmoothingGameMode)


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_9_PROLOG
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_RPC_WRAPPERS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_INCLASS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_INCLASS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

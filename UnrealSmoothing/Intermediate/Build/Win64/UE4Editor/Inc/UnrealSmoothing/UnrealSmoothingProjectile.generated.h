// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef UNREALSMOOTHING_UnrealSmoothingProjectile_generated_h
#error "UnrealSmoothingProjectile.generated.h already included, missing '#pragma once' in UnrealSmoothingProjectile.h"
#endif
#define UNREALSMOOTHING_UnrealSmoothingProjectile_generated_h

#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingProjectile(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingProjectile(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/UnrealSmoothing"), NO_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingProjectile(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingProjectile(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/UnrealSmoothing"), NO_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealSmoothingProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealSmoothingProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealSmoothingProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealSmoothingProjectile(AUnrealSmoothingProjectile&&); \
	NO_API AUnrealSmoothingProjectile(const AUnrealSmoothingProjectile&); \
public:


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealSmoothingProjectile(AUnrealSmoothingProjectile&&); \
	NO_API AUnrealSmoothingProjectile(const AUnrealSmoothingProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealSmoothingProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnrealSmoothingProjectile)


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ProjectileMesh() { return STRUCT_OFFSET(AUnrealSmoothingProjectile, ProjectileMesh); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AUnrealSmoothingProjectile, ProjectileMovement); }


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_12_PROLOG
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_RPC_WRAPPERS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_INCLASS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_INCLASS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

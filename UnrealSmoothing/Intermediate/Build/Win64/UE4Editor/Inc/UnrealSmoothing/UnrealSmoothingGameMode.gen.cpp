// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "UnrealSmoothingGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnrealSmoothingGameMode() {}
// Cross Module References
	UNREALSMOOTHING_API UClass* Z_Construct_UClass_AUnrealSmoothingGameMode_NoRegister();
	UNREALSMOOTHING_API UClass* Z_Construct_UClass_AUnrealSmoothingGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_UnrealSmoothing();
// End Cross Module References
	void AUnrealSmoothingGameMode::StaticRegisterNativesAUnrealSmoothingGameMode()
	{
	}
	UClass* Z_Construct_UClass_AUnrealSmoothingGameMode_NoRegister()
	{
		return AUnrealSmoothingGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AUnrealSmoothingGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_UnrealSmoothing,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "UnrealSmoothingGameMode.h" },
				{ "ModuleRelativePath", "UnrealSmoothingGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AUnrealSmoothingGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AUnrealSmoothingGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUnrealSmoothingGameMode, 2016653607);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUnrealSmoothingGameMode(Z_Construct_UClass_AUnrealSmoothingGameMode, &AUnrealSmoothingGameMode::StaticClass, TEXT("/Script/UnrealSmoothing"), TEXT("AUnrealSmoothingGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUnrealSmoothingGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALSMOOTHING_UnrealSmoothingPawn_generated_h
#error "UnrealSmoothingPawn.generated.h already included, missing '#pragma once' in UnrealSmoothingPawn.h"
#endif
#define UNREALSMOOTHING_UnrealSmoothingPawn_generated_h

#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_RPC_WRAPPERS
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingPawn(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingPawn(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingPawn, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/UnrealSmoothing"), NO_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealSmoothingPawn(); \
	friend UNREALSMOOTHING_API class UClass* Z_Construct_UClass_AUnrealSmoothingPawn(); \
public: \
	DECLARE_CLASS(AUnrealSmoothingPawn, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/UnrealSmoothing"), NO_API) \
	DECLARE_SERIALIZER(AUnrealSmoothingPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealSmoothingPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealSmoothingPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealSmoothingPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealSmoothingPawn(AUnrealSmoothingPawn&&); \
	NO_API AUnrealSmoothingPawn(const AUnrealSmoothingPawn&); \
public:


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealSmoothingPawn(AUnrealSmoothingPawn&&); \
	NO_API AUnrealSmoothingPawn(const AUnrealSmoothingPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealSmoothingPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealSmoothingPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnrealSmoothingPawn)


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShipMeshComponent() { return STRUCT_OFFSET(AUnrealSmoothingPawn, ShipMeshComponent); } \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(AUnrealSmoothingPawn, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AUnrealSmoothingPawn, CameraBoom); }


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_9_PROLOG
#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_RPC_WRAPPERS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_INCLASS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_INCLASS_NO_PURE_DECLS \
	UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealSmoothing_Source_UnrealSmoothing_UnrealSmoothingPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
